﻿using System;
using System.Threading;
using WarriorWars.Enum;
using WarriorWars.Equipment;

namespace WarriorWars
{
    class Program
    {
        static Random rand = new Random();
        static void Main(string[] args)
        {

            Warrior goodGuy = new Warrior("goodGuy", Faction.GoodGuy);
            Warrior badGuy = new Warrior("badGuy", Faction.BadGuy);

            while(goodGuy.IsAlive && badGuy.IsAlive)
            {
               if(rand.Next(0, 10) < 5)
                {
                    goodGuy.Attack(badGuy);
                }
               else
                {
                    badGuy.Attack(goodGuy);
                }

                Thread.Sleep(500);

            }
        }
    }
}
